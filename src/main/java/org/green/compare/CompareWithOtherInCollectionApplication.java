package org.green.compare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompareWithOtherInCollectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompareWithOtherInCollectionApplication.class, args);
	}

}
