package org.green.compare.main;

import lombok.extern.slf4j.Slf4j;
import org.green.compare.model.CDR;
import org.green.compare.model.CallDirection;
import org.green.compare.model.CallType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;


@Slf4j
@Component
public class Main implements CommandLineRunner {

    @Override
    public void run(String... args) {
//        List<CDR> cdrList = generateCDR();
        List<CDR> cdrList = generateCDR1();

//        cdrList.forEach(cdr -> {
//            log.info("{}", cdr);
//        });

        Map<String, Integer> coordinatorCounter = new HashMap<>();
        Map<String, Integer> participantCounter = new HashMap<>();

        for (int i = 0; i < cdrList.size(); i++) {
            log.info("i: {}. CDR: {}", i, cdrList.get(i));
            String msisdnA = cdrList.get(i).getMsisdnA();

            Integer coordinatorCountMsisdnA = coordinatorCounter.get(msisdnA);
            Integer participantCountMsisdnA = participantCounter.get(msisdnA);

            if (participantCountMsisdnA == null) {
                participantCounter.put(msisdnA, 1);
            } else {
                participantCounter.put(msisdnA, participantCountMsisdnA + 1);
            }

            if (coordinatorCountMsisdnA != null) {
                log.info("i: {}. Coordinator counting for msisdn {} is already done!", i, msisdnA);
                continue;
            }

            coordinatorCountMsisdnA = 0;
            for (int j = 0; j < cdrList.size(); j++) {
                if (i == j) {
                    log.info("i: {}. j: {}. Skip comparing to itself!", i, j);
                    continue;
                }

                log.info("i: {}. j: {}. Compared to CDR: {}", i, j, cdrList.get(j));
                if (cdrList.get(i).getMsisdnA().equals(cdrList.get(j).getMsisdnB())) {
                    coordinatorCountMsisdnA++;
                }
            }
            coordinatorCounter.put(msisdnA, coordinatorCountMsisdnA);
        }

        log.info("Coordinator counter: {}", coordinatorCounter);
        log.info("Participant counter: {}", participantCounter);
    }

    private List<CDR> generateCDR() {
        List<CDR> cdrList = new ArrayList<>();
        CDR cdr1 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1576730808,
                                   1200,
                                   CallDirection.IN,
                                   "7435514525962",
                                   "4268059936922").build();

        CDR cdr2 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1576817208,
                                   1000,
                                   CallDirection.IN,
                                   "4268059936922",
                                   "7435514525962").build();

        CDR cdr3 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1576903608,
                                   900,
                                   CallDirection.IN,
                                   "4268059936922",
                                   "7435514525962").build();

        CDR cdr4 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1576990008,
                                   900,
                                   CallDirection.IN,
                                   "4268059936922",
                                   "7435514525962").build();

        CDR cdr5 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1577076408,
                                   800,
                                   CallDirection.IN,
                                   "7435514525962",
                                   "4268059936922").build();

        cdrList.add(cdr1);
        cdrList.add(cdr2);
        cdrList.add(cdr3);
        cdrList.add(cdr4);
        cdrList.add(cdr5);
        return cdrList;
    }

    private List<CDR> generateCDR1() {
        List<CDR> cdrList = new ArrayList<>();
        CDR cdr1 = new CDR.Builder(UUID.randomUUID(),
                                   "Movilnet",
                                   CallType.VOICE,
                                   1576730808,
                                   1200,
                                   CallDirection.IN,
                                   "7435514525962",
                                   "4268059936922").build();

        cdrList.add(cdr1);
        return cdrList;
    }
}
