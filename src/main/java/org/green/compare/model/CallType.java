/*
 * Copyright (c) 2007-2013 trovicor GmbH. All rights reserved.
 *
 * Project: Monitoring Center Next Generation (MCng)
 * COMPANY CONFIDENTIAL.
 */
package org.green.compare.model;


/**
 * Identifies CDR types
 *
 * @author cd01 & dm01 03.23.15
 * @since 04.12.13
 */
public enum CallType {
    VOICE,
    SMS,
    DATA,
    UNKNOWN;

    public static CallType of(String value) {
        if (value == null) return UNKNOWN;

        switch (value) {
            case "V": // legacy
            case "v": // legacy
            case "VOICE": return VOICE;
            case "S": // legacy
            case "s": // legacy
            case "SMS": return SMS;
            case "D": // legacy
            case "d": // legacy
            case "DATA": return DATA;
            default: break;
        }

        return UNKNOWN;
    }
}
