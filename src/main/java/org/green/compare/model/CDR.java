package org.green.compare.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Canonical format of Call Detail Record.
 * Use inner Builder for creating the object of CDR.
 *
 * @author dm01
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class CDR implements Canonical, Comparable<CDR>, Serializable {
    public static final String StartFormatPattern = "yyyyMMddHHmmss";
    public static final DateTimeFormatter StartFormatter = DateTimeFormatter.ofPattern(StartFormatPattern).withZone(ZoneOffset.UTC);

    private final UUID uuid;
    private final String provider;
    private final CallType callType;
    private final long start;
    private final int duration;
    private final int direction;
    private final String msisdnA;
    private final String msisdnB;
    private String msisdnC;
    private String imsiA;
    private String imsiB;
    private String imeiA;
    private String imeiB;
    private String status;
    private String countryA;
    private String countryB;
    private String smsContent;
    private List<String> voiceContent;
    private CDRProperties properties;

    private CDR(UUID uuid, String provider, CallType callType, long start, int duration, CallDirection direction, String msisdnA, String msisdnB) {
        this.uuid = uuid;
        this.provider = provider;
        this.callType = callType;
        this.start = start;
        this.duration = duration;
        this.direction = direction.ordinal();
        this.msisdnA = msisdnA;
        this.msisdnB = msisdnB;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getProvider() {
        return provider;
    }

    public CallType getCallType() {
        return callType;
    }

    public long getStart() {
        return start;
    }

    public int getDuration() {
        return duration;
    }

    public int getDirection() {
        return direction;
    }

    public CallDirection getDirectionEnum() {
        return CallDirection.of(direction);
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public String getMsisdnC() {
        return msisdnC;
    }

    public String getImsiA() {
        return imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public String getImeiA() {
        return imeiA;
    }

    public String getImeiB() {
        return imeiB;
    }

    public String getStatus() {
        return status;
    }

    public String getCountryA() {
        return countryA;
    }

    public String getCountryB() {
        return countryB;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public List<String> getVoiceContent() {
        return voiceContent;
    }

    public CDRProperties getProperties() {
        return properties;
    }

    public String getStartFormatted() {
        return getStartAsDateTime().format(StartFormatter);
    }

    static public long formattedStartToTimestamp(String start) {
        return ZonedDateTime.parse(start, StartFormatter).toInstant().toEpochMilli();
    }

    public ZonedDateTime getStartAsDateTime() {
        return Instant.ofEpochMilli(start).atZone(ZoneOffset.UTC);
    }

    @Override
    public int compareTo(CDR other) {
        return Long.compare(other.start, start);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CDR cdr = (CDR) o;
        return start == cdr.start &&
               duration == cdr.duration &&
               direction == cdr.direction &&
               callType == cdr.callType &&
               Objects.equals(uuid, cdr.uuid) &&
               Objects.equals(provider, cdr.provider) &&
               Objects.equals(msisdnA, cdr.msisdnA) &&
               Objects.equals(msisdnB, cdr.msisdnB) &&
               Objects.equals(msisdnC, cdr.msisdnC) &&
               Objects.equals(imsiA, cdr.imsiA) &&
               Objects.equals(imsiB, cdr.imsiB) &&
               Objects.equals(imeiA, cdr.imeiA) &&
               Objects.equals(imeiB, cdr.imeiB) &&
               Objects.equals(status, cdr.status) &&
               Objects.equals(countryA, cdr.countryA) &&
               Objects.equals(countryB, cdr.countryB) &&
               Objects.equals(smsContent, cdr.smsContent) &&
               Objects.equals(voiceContent, cdr.voiceContent) &&
               Objects.equals(properties, cdr.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, provider, callType, start, duration, direction, msisdnA, msisdnB, msisdnC, imsiA, imsiB, imeiA, imeiB,
                status,  countryA, countryB, smsContent, voiceContent, properties);
    }

    @Override
    public String toString() {
        return "CDR{" +
               "uuid=" + uuid +
               ", provider='" + provider + '\'' +
               ", callType=" + callType +
               ", start=" + start +
               ", duration=" + duration +
               ", direction=" + direction +
               ", msisdnA='" + msisdnA + '\'' +
               ", msisdnB='" + msisdnB + '\'' +
               ", msisdnC='" + msisdnC + '\'' +
               ", imsiA='" + imsiA + '\'' +
               ", imsiB='" + imsiB + '\'' +
               ", imeiA='" + imeiA + '\'' +
               ", imeiB='" + imeiB + '\'' +
               ", status='" + status + '\'' +
               ", countryA='" + countryA + '\'' +
               ", countryB='" + countryB + '\'' +
               ", smsContent='" + smsContent + '\'' +
               ", voiceContent=" + voiceContent +
               ", properties=" + properties +
               '}';
    }

    /**
     * Implementation of builder pattern for CDR..
     */
    public static class Builder {

        private final UUID uuid;
        private String provider;
        private CallType callType;
        private long start;
        private int duration;
        private CallDirection direction;
        private String msisdnA;
        private String msisdnB;
        private String msisdnC;
        private String imsiA;
        private String imsiB;
        private String imeiA;
        private String imeiB;
        private String status;
        private String countryA;
        private String countryB;
        private String smsContent;
        private List<String> voiceContent;
        private CDRProperties properties;

        public Builder(UUID uuid, String provider, CallType callType, long start, int duration, CallDirection direction, String msisdnA, String msisdnB) {

            this.uuid = uuid;
            this.provider = provider;
            this.callType = callType;
            this.start = start;
            this.duration = duration;
            this.direction = direction;
            this.msisdnA = msisdnA;
            this.msisdnB = msisdnB;
        }

        public Builder(UUID uuid, String provider, CallType callType, long start, int duration, int direction, String msisdnA, String msisdnB) {
            this(uuid, provider, callType, start, duration, CallDirection.of(direction), msisdnA, msisdnB);
        }

        public Builder(UUID uuid, String provider, String callType, long start, int duration, int direction, String msisdnA, String msisdnB) {
            this(uuid, provider, CallType.of(callType), start, duration, direction, msisdnA, msisdnB);
        }


        public Builder(CDR cdr) {
            this(cdr.uuid, cdr.provider, cdr.callType, cdr.start, cdr.duration, cdr.direction, cdr.msisdnA, cdr.msisdnB);

            this.setMsisdnC(cdr.msisdnC)
                .setImsiA(cdr.imsiA)
                .setImsiB(cdr.imsiB)
                .setImeiA(cdr.imeiA)
                .setImeiB(cdr.imeiB)
                .setStatus(cdr.status)
                .setCountryA(cdr.countryA)
                .setCountryB(cdr.countryB)
                .setSmsContent(cdr.smsContent)
                .setVoiceContent(cdr.voiceContent)
                .setProperties(cdr.properties);
        }

        public Builder setProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public Builder setCallType(CallType callType) {
            this.callType = callType;
            return this;
        }

        public Builder setStart(long start) {
            this.start = start;
            return this;
        }

        public Builder setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public Builder setDirection(CallDirection direction) {
            this.direction = direction;
            return this;
        }

        public Builder setDirection(int direction) {
            this.direction = CallDirection.of(direction);
            return this;
        }

        public Builder setMsisdnA(String msisdnA) {
            this.msisdnA = msisdnA;
            return this;
        }

        public Builder setMsisdnB(String msisdnB) {
            this.msisdnB = msisdnB;
            return this;
        }

        public Builder setMsisdnC(String msisdnC) {
            this.msisdnC = msisdnC;
            return this;
        }

        public Builder setImsiA(String imsiA) {
            this.imsiA = imsiA;
            return this;
        }

        public Builder setImsiB(String imsiB) {
            this.imsiB = imsiB;
            return this;
        }

        public Builder setImeiA(String imeiA) {
            this.imeiA = imeiA;
            return this;
        }

        public Builder setImeiB(String imeiB) {
            this.imeiB = imeiB;
            return this;
        }

        public Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder setCountryA(String countryA) {
            this.countryA = countryA;
            return this;
        }

        public Builder setCountryB(String countryB) {
            this.countryB = countryB;
            return this;
        }

        public Builder setSmsContent(String smsContent) {
            this.smsContent = smsContent;
            return this;
        }

        public Builder setVoiceContent(List<String> voiceContent) {
            this.voiceContent = voiceContent;
            return this;
        }

        public Builder setProperties(CDRProperties properties) {
            this.properties = properties;
            return this;
        }

        public CDR build() {
            CDR cdr = new CDR(uuid, provider, callType, start, duration, direction, msisdnA, msisdnB);

            cdr.msisdnC = this.msisdnC;
            cdr.imsiA = this.imsiA;
            cdr.imsiB = this.imsiB;
            cdr.imeiA = this.imeiA;
            cdr.imeiB = this.imeiB;
            cdr.status = this.status;
            cdr.countryA = this.countryA;
            cdr.countryB = this.countryB;
            cdr.smsContent = this.smsContent;
            cdr.voiceContent = this.voiceContent;
            cdr.properties = this.properties;

            return cdr;
        }
    }
}
