package org.green.compare.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.*;


/**
 * Actor model. The only supported identity type in this model is MSISDN.
 */
@Slf4j
@Data
public class Actor implements Comparable<Actor> {

    private final String targetId;
    private final int participantCount;
    private final int coordinatorCount;
    private final Set<String> msisdns = new HashSet<>();
    private final Set<String> imeis = new HashSet<>();
    private final Set<String> imsis = new HashSet<>();
    private final long timestamp;

    /**
     * Returns a set of msisdns used by this actor.
     *
     * @return set of msisdns
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Actor actor = (Actor) o;
        return Objects.equals(targetId, actor.getTargetId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetId);
    }

    @Override
    public int compareTo(Actor other) {
        int count = coordinatorCount + participantCount;
        int otherCount = other.coordinatorCount + other.participantCount;

        if (count > otherCount) {
            return 1;
        }
        else if (count < otherCount) {
            return -1;
        }
        else return 0;
    }
}
