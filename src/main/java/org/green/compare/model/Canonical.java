package org.green.compare.model;

import java.util.UUID;


public interface Canonical  {
    /**
     * @return unique identifier of canonical type
     */
    UUID getUuid();
}
