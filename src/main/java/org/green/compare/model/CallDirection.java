package org.green.compare.model;

public enum CallDirection {
    UNKNOWN,
    IN,
    OUT;

    public static CallDirection of(int value) {
        switch (value) {
            case 1: return IN;
            case 2: return OUT;
            default: break;
        }

        return UNKNOWN;
    }

    public static CallDirection of(String value) {
        switch (value) {
            case "IN":  return IN;
            case "OUT": return OUT;
            default: break;
        }

        return UNKNOWN;
    }
}
